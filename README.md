# nbwmon
ncurses bandwidth monitor, fork of https://github.com/causes-/nbwmon

## Features
* print transmitted and received bandwidth graphs
* print current, maximum, average and minimum transfer speeds
* print total traffic received and transmitted
* scale to full window on startup and resize
* detect active network interface
* support for multiple units
* support for colors

## Runtime Options
| key | action                |
| :-: | :---                  |
| s   | use SI / IEC units    |
| b   | use Byte/s / Bit/s    |
| m   | scale graph minimum   |
| g   | show global maximum   |
| +   | increase redraw delay |
| -   | decrease redraw delay |

## Options
| option       |                            |
| :---         | :---                       |
| -h           | show help                  |
| -v           | show version               |
| -C           | disable colors             |
| -s           | use SI units               |
| -b           | use Bit/s                  |
| -m           | scale graph minimum        |
| -g           | show global maximum        |
| -d seconds   | redraw delay in  seconds   |
| -i interface | select   network interface |

## Install
### Dependencies
- ncurses development files
- and one of:
	- [meson](https://mesonbuild.com/) and [ninja](https://ninja-build.org/) or [samurai](https://github.com/michaelforney/samurai)
	- [gnu make](https://www.gnu.org/software/make/)
	- [cmake](https://cmake.org/), tested with 3.13 and later
### Building
#### with meson and ninja
You can replace "`ninja`" with "`samu`", to use samurai.
```
$ meson setup --buildtype=release build
$ ninja -C build
# ninja -C build install
```
#### with gnu make
You have to use "`gmake`" on the BSDs.
```
$ make
# make install
```
#### with cmake
Works with BSD make.
Also works with ninja / samurai, just use "`cmake -GNinja ..`" and call ninja / samu.
There are currently no install instructions for cmake builds.
```
$ mkdir build && cd build
$ cmake ..
$ make
```
